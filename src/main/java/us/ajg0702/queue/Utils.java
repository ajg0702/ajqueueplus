package us.ajg0702.queue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.logging.Logger;

import me.activated.core.plugin.AquaCoreAPI;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.query.QueryOptions;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Utils {
	public static List<String> getPermissions(ProxiedPlayer p) {

		boolean debug = Manager.getInstance().pl.getConfig().getBoolean("priority-queue-debug");

		Logger l = Manager.getInstance().pl.getLogger();

		if(ProxyServer.getInstance().getPluginManager().getPlugin("LuckPerms") != null) {
			if(debug) {
				l.info("Using luckperms");
			}

			LuckPerms api = LuckPermsProvider.get();
			
			User user = api.getUserManager().getUser(p.getUniqueId());
			
			SortedSet<Node> nodes = user.resolveDistinctInheritedNodes(QueryOptions.defaultContextualOptions());
			
			List<String> perms = new ArrayList<>();
			
			Iterator<Node> it = nodes.iterator();
			while(it.hasNext()) {
				Node node = it.next();
				if(!node.getType().equals(NodeType.PERMISSION)) continue;
				if(!node.getValue()) continue;
				perms.add(node.getKey());
			}
			
			return perms;
		}
		if(ProxyServer.getInstance().getPluginManager().getPlugin("AquaCore") != null) {
			if(debug) l.info("Using AquaCore");
			return AquaCoreAPI.INSTANCE.getGlobalPlayer(p.getUniqueId()).getPermissions();
		}

		if(debug) l.info("Using default permissions");

		return new ArrayList<>(p.getPermissions());
	}
}
