package us.ajg0702.queue;

import us.ajg0702.utils.bungee.BungeeConfig;

import java.util.List;

@SuppressWarnings("unused")
public class AliasManager {
	Main pl;
	BungeeConfig configa;
	public AliasManager(Main pl) {
		this.pl = pl;
		this.configa = pl.config;
	}
	
	public String getAlias(String server) {
		List<String> aliasesraw = configa.getStringList("server-aliases");
		for(String aliasraw : aliasesraw) {
			String realname = aliasraw.split(":")[0];
			if(!realname.equalsIgnoreCase(server)) continue;
			return aliasraw.split(":")[1];
		}
		return server;
	}
	public String getServer(String alias) {
		List<String> aliasesraw = configa.getStringList("server-aliases");
		for(String aliasraw : aliasesraw) {
			String salias = aliasraw.split(":")[1];
			if(!alias.equalsIgnoreCase(salias)) continue;
			return aliasraw.split(":")[0];
		}
		return alias;
	}
}
