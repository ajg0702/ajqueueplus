package us.ajg0702.queue;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import net.md_5.bungee.api.connection.ProxiedPlayer;

@SuppressWarnings("unused")
public class Logic {


	static boolean isp = true;
	
	public static void priorityLogic(List<ProxiedPlayer> list, String server, ProxiedPlayer p) {
		if(p.hasPermission("ajqueue.bypass")) {
			list.add(0, p);
			Manager.getInstance().sendPlayers(server);
			return;
		}
		if(p.hasPermission("ajqueue.serverbypass."+server)) {
			list.add(0, p);
			Manager.getInstance().sendPlayers(server);
			return;
		}
		int highestPerm = getHighestPriority(p);
		int highestServerPerm = getHighestPriority(p, "ajqueue.serverpriority."+server+".");

		boolean debug = Manager.getInstance().pl.getConfig().getBoolean("priority-queue-debug");

		Logger l = Manager.getInstance().pl.getLogger();
		if(debug) {
			l.info("highestPerm: "+highestPerm);
			l.info("highestServerPerm: "+highestServerPerm);
		}

		if(highestPerm <= 0 && highestServerPerm <= 0) {
			list.add(p);
			return;
		}
		int fi = 0;
		boolean addToEnd = true;
		for(int i = 0; i < list.size(); i++) {
			ProxiedPlayer pl = list.get(i);
			int plHP = getHighestPriority(pl);
			int plSHP = getHighestPriority(pl, "ajqueue.serverpriority."+server+".");
			if(plHP < highestPerm || plSHP < highestServerPerm) {
				list.add(i, p);
				fi = i;
				addToEnd = false;
			}
		}
		if(addToEnd) {
			list.add(p);
		}
		if(debug) {
			l.info("put: "+fi);
		}


	}
	
	
	private static int getHighestPriority(ProxiedPlayer p) {
		return getHighestPriority(p, "ajqueue.priority.");
	}
	private static int getHighestPriority(ProxiedPlayer p, String permprefix) {
		Collection<String> perms = Utils.getPermissions(p);
		Iterator<String> it = perms.iterator();
		String highestPerm = permprefix+"0";
		while(it.hasNext()) {
			String perm = it.next();
			if(!perm.startsWith(permprefix)) continue;
			if(highestPerm.isEmpty()) {
				highestPerm = perm;
				continue;
			}
			int level = Integer.parseInt(perm.substring(permprefix.length()));
			int highestlevel = Integer.parseInt(highestPerm.substring(permprefix.length()));
			if(level > highestlevel) {
				highestPerm = perm;
			}
		}
		return Integer.parseInt(highestPerm.substring(permprefix.length()));
	}
}
