plugins {
    java
    id("com.github.johnrengelman.shadow").version("6.1.0")
    `maven-publish`
}

repositories {
    mavenLocal()
    maven { url = uri("https://gitlab.com/api/v4/projects/19978391/packages/maven") }
    maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots") }
    maven { url = uri("https://repo.maven.apache.org/maven2/") }
    maven { url = uri("https://repo.ajg0702.us/releases") }
}

group = "us.ajg0702"
version = "1.9.8"

dependencies {
    implementation("us.ajg0702:ajQueue:$version")
    implementation("us.ajg0702:ajUtils:1.0.0")
    compileOnly("net.md-5:bungeecord-api:1.14-SNAPSHOT")
    compileOnly("net.luckperms:api:5.0")
    compileOnly(files("libs/AquaCoreAPI.jar"))
}

tasks.shadowJar {
    relocate("us.ajg0702.utils", "us.ajg0702.queue.utils")
    archiveFileName.set("${baseName}-${version}.${extension}")
}


publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}
